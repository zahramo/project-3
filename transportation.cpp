#include <iostream>
#include <vector>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>
#include <thread>
#include <ctime>
#include <cmath>
#include <ctime>
#include <chrono>

using namespace std;
using namespace std::chrono;

typedef struct Monitor 
{
    sem_t okToEntry;
    int isCarInRoad;
    int numOfCarsWaiting;
} Monitor;

typedef struct Road
{
    string origin;
    string destination;
    int hardness;
    Monitor monitor;
} Road;

typedef struct Car
{
    vector<int> routeRoads;
    int carNumber;
    int pathNumber;
    int contaminationRate;
    string outFileName;
    vector<string> outFileData;
} Car;

vector<Road> cityRoads;
vector<Car> cars;
long int totalEmission = 0;
sem_t okToAdd; 

vector<string> readFromFile(string fileName)
{
    fstream f;
    f.open(fileName, ios::in);
    vector<string> fileData;
    string data;
    while(f >> data)
    {
        fileData.push_back(data);
    }
    f.close();
    return fileData;
}

vector<string> splitByChar(string data, char c)
{
    vector<string> result;
    string part;
    for(int i=0; i<data.length(); i++)
    {
        if(data[i] == ' ') break;
        if(data[i] != c)
        {
            part = part + data[i];
        }
        else
        {
            if(part.length() > 0) 
            {
                result.push_back(part);
                part = "";
            }
        }
    }
    if (part != "")
    {
        result.push_back(part);
    }
    return result;
}

int findIndexOfRoad(string origin, string destination)
{
    for(int i=0; i<cityRoads.size(); i++)
    {
        if(cityRoads[i].origin == origin && cityRoads[i].destination == destination)
        {
            return i;
        }
    }
    return -1;
}

void parsInputs(vector<string> inputs)
{
    int state = 0;
    int index = 0;
    int car_index = 0;

    for(int i=0; i<inputs.size(); i++)
    {
        if(inputs[i] == "#")
        {
            state = 1;
            index = i;
        }
        else if(state == 0)
        {
            Road road;
            vector<string> info = splitByChar(inputs[i], '-');
            road.origin = info[0];
            road.destination = info[1];
            road.hardness = stoi(info[2]);
            cityRoads.push_back(road);
        }
        else if(state == 1)
        {
            vector<string> info = splitByChar(inputs[i], '-');
            vector<int> roads;

            for(int j=0; j<info.size()-1; j++)
            {
                roads.push_back(findIndexOfRoad(info[j],info[j+1]));
            }

            i++;
            for(int j=0; j<stoi(inputs[i]); j++)
            {
                Car car;
                car.carNumber = car_index ++;   
                car.routeRoads = roads;
                car.pathNumber = (i - index - 2)/2;
                car.contaminationRate = rand() % 10 + 1;
                cars.push_back(car);
            }
        }
    }
}

void printRoadsAndCarsInfo()
{
    for(int i=0; i<cityRoads.size(); i++)
    {
        cout<<cityRoads[i].origin<<" "<<cityRoads[i].destination<<" "<<cityRoads[i].hardness<<endl;
    }
    for(int i=0; i<cars.size(); i++)
    {
        // cout<<cars[i].outFileName<<" : ";
        // for(int j=0; j<cars[i].outFileData.size(); j++)
        // {
            //  cout<<cars[i].outFileData[j]<<" ";
        // }
        // cout<<endl;
        // cars[i].contaminationRate
        cout<<cars[i].carNumber<<" - "<<cars[i].pathNumber<<" - "<<" - ";
    
        for(int j=0; j<cars[i].routeRoads.size(); j++)
        {
            cout<<cars[i].routeRoads[j]<<" ";
        }
        cout<<endl;
    }
}

int calcuteTheAmountOfPollutionProduced(int h, int p)
{
    //h<=4?
    int sum = 0;
    for(int k=0; k< 1e7; k++)
    {
        sum += k / 1e6 * p * h;
    }
    return sum;
}


void startTravel(Car* car)
{
    //time_t timeIn;
    //time_t timeOut;

    int pollutionProduced = 0;
    string data;

    vector<int> indexOfRoads = car->routeRoads;
    
    for(int i=0; i<indexOfRoads.size(); i++)
    {
        //timeIn = time(nullptr);
        
        sem_wait(&(cityRoads[indexOfRoads[i]].monitor.okToEntry));
        auto timeIn = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
            
        pollutionProduced = calcuteTheAmountOfPollutionProduced(cityRoads[indexOfRoads[i]].hardness, car->contaminationRate);
        //timeOut = time(nullptr);
        auto timeOut = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        data = cityRoads[indexOfRoads[i]].origin + ", " + to_string(timeIn.count()) + ", " + cityRoads[indexOfRoads[i]].destination + ", " + to_string(timeOut.count()) + ", " + to_string(pollutionProduced) + ", " + to_string(totalEmission);
        car->outFileData.push_back(data);
        sem_post(&(cityRoads[indexOfRoads[i]].monitor.okToEntry));

        sem_wait(&(okToAdd));
        totalEmission += pollutionProduced;
        sem_post(&(okToAdd));
    }
}

void createCarThreads()
{
    vector<thread> carThreads;
    for(int i=0; i<cars.size(); i++)
    {
        cars[i].outFileName = to_string(cars[i].pathNumber) + "-" + to_string(cars[i].carNumber);
        carThreads.push_back(thread(startTravel, &cars[i]));
    }
    for(int i=0; i<carThreads.size(); i++)
    {
        if (carThreads[i].joinable())
            carThreads[i].join();
    }
}

void initialMonitors()
{
    for(int i=0; i<cityRoads.size(); i++)
    {
        cityRoads[i].monitor.isCarInRoad = 0;
        cityRoads[i].monitor.numOfCarsWaiting = 0;
        if(sem_init(&(cityRoads[i].monitor.okToEntry), 0, 1) == 1)
        {
            cout<<"Unable to initialize semaphores\n";
        }

    }
}

void destroyMonitors()
{   
    for(int i=0; i<cityRoads.size(); i++)
    {
        sem_destroy(&(cityRoads[i].monitor.okToEntry));
    }
}

void writeDataInFile(Car car)
{
    string data;
    for(int i=0; i<car.outFileData.size(); i++)
    {
        data = data + car.outFileData[i] + "\n";
    }
    string outFileName = car.outFileName;
    ofstream outFile(outFileName);
    outFile<< data;
    outFile.close();
}

void generateOutputFiles()
{
    for(int i=0; i<cars.size(); i++)
    {
        writeDataInFile(cars[i]);
    }   
}

void initialEmissionSem()
{
    if(sem_init(&(okToAdd), 0, 1) == 1)
    {
        cout<<"Unable to initialize semaphores\n";
    }
}


int main(int argc, const char * argv[])
{
    srand(time(NULL));
    vector<string> inputs = readFromFile("inputs.txt");
    parsInputs(inputs);
    //printRoadsAndCarsInfo();
    
    initialMonitors();
    initialEmissionSem();
    createCarThreads();
    destroyMonitors();
    // printRoadsAndCarsInfo();
    generateOutputFiles();
}